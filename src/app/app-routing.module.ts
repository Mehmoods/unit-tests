import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsPageComponent } from './components/components-page/components-page.component';
import { SingleComponentComponent } from './components/single-component/single-component.component';


const routes: Routes = [
  { path: '', component: ComponentsPageComponent },
  { path: ':id', component: SingleComponentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
