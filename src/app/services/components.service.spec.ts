import { TestBed, inject, fakeAsync, flush } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentsService } from './components.service';
import { MockData } from '../models/mock-data';

describe('ComponentsService', () => {
  let baseUrl: string;
  let httpTestingController: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ComponentsService]
  }));

  beforeEach(() => {
    baseUrl = `http://jsonplaceholder.typicode.com/comments`;
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([ComponentsService], (service: ComponentsService) => {
    expect(service).toBeTruthy();
  }));

  it('should detect baseUrl', inject([ComponentsService], (service: ComponentsService) => {
    expect(service.baseUrl).toEqual(baseUrl);
  }));

  it('should be get Components', fakeAsync(inject([ComponentsService], (service: ComponentsService) => {
    service.fetchComponents().subscribe(data => {
      expect(data).toEqual(MockData);
    });
    const req = httpTestingController.expectOne(`${baseUrl}?_limit=14`);
    expect(req.request.method).toEqual('GET');
    req.flush(MockData);
    flush();
  })));

  it('should be push new Component', fakeAsync(inject([ComponentsService], (service: ComponentsService) => {
    const mockAddComponents = MockData[0];

    service.addComponent({ topicId: 1 })
      .subscribe(mockData => {
        expect(mockData.name).toEqual('id labore ex et quam laborum');
      });
    const req = httpTestingController.expectOne(baseUrl);
    expect(req.request.method).toEqual('POST');
    req.flush(mockAddComponents);
  })));

});
