import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Components } from '../models/component-model';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {
  baseUrl = `http://jsonplaceholder.typicode.com/comments`;

  constructor(private http: HttpClient) { }

  fetchComponents(): Observable<Components[]> {
    return this.http.get<Components[]>(this.baseUrl, {
      params: new HttpParams().set('_limit', '14')
    })
      .pipe(
        filter((components: Components[] | any) => {
          components = components.filter(data => {
            delete data.postId;
            return data;
          });
          return components;
        })
      );
  }

  fetchSingleComponent(id): Observable<Components> {
    return this.http.get<Components>(`${this.baseUrl}/${id}`);
  }

  addComponent(component): Observable<Components> {
    return this.http.post<Components>(this.baseUrl, component);
  }
}
