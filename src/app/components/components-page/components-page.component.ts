import { Component, OnInit } from '@angular/core';
import { ComponentsService } from 'src/app/services/components.service';
import { Observable } from 'rxjs';
import { Components } from 'src/app/models/component-model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-components-page',
  templateUrl: './components-page.component.html',
  styleUrls: ['./components-page.component.scss']
})
export class ComponentsPageComponent implements OnInit {

  components: Components[];
  componentsForm: FormGroup;
  constructor(private componentsService: ComponentsService, private fb: FormBuilder) { }

  ngOnInit() {
    this.fetchComponents();
    this.createForm();
  }

  fetchComponents() {
    this.componentsService.fetchComponents()
      .subscribe((components) => {
        this.components = components;
      });
  }

  createForm() {
    this.componentsForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      body: ['', [Validators.required]]
    });
  }

  submitForm() {
    this.componentsForm.value;
    this.componentsService.addComponent(this.componentsForm.value)
      .subscribe(data => {
        this.components.push(data);
        this.componentsForm.reset();
      });
  }

}
