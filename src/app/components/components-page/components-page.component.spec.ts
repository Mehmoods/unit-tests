import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentsPageComponent } from './components-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MockData } from '../../models/mock-data';
import { ComponentsService } from 'src/app/services/components.service';
import { RouterModule } from '@angular/router';
import { of } from 'rxjs';

describe('ComponentsPageComponent', () => {
  let component: ComponentsPageComponent;
  let fixture: ComponentFixture<ComponentsPageComponent>;
  let componentsService: ComponentsService;
  let spy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterModule
      ],
      declarations: [ComponentsPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsPageComponent);
    component = fixture.componentInstance;
    componentsService = fixture.debugElement.injector.get(ComponentsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call components service', () => {
    component.fetchComponents();
    spy = spyOn(componentsService, 'fetchComponents').and.returnValue(of(MockData));
    expect(spy.calls.any).toBeTruthy();
  });

  it('should push new components in server', () => {
    component.fetchComponents();
    spy = spyOn(componentsService, 'addComponent').and.returnValue(of(MockData[1]));
    expect(spy.calls.any).toBeTruthy();
  });

});

