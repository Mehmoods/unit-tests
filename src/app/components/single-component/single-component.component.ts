import { Component, OnInit } from '@angular/core';
import { ComponentsService } from 'src/app/services/components.service';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Components } from 'src/app/models/component-model';

@Component({
  selector: 'app-single-component',
  templateUrl: './single-component.component.html',
  styleUrls: ['./single-component.component.scss']
})
export class SingleComponentComponent implements OnInit {

  singleComponent: Components[] = [];
  constructor(private componentsService: ComponentsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.fetchSingleComponent();
  }

  fetchSingleComponent() {
    this.componentsService.fetchSingleComponent(this.route.snapshot.params.id)
      .subscribe((component) => {
        this.singleComponent.push(component);
      });
  }

}
